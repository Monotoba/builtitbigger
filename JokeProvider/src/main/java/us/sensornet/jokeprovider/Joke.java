package us.sensornet.jokeprovider;

public class Joke {
    private String mJoke;
    private String mPunchLine;

    public Joke() {

    }

    public Joke(String joke, String punchLine) {
        mJoke = joke;
        mPunchLine = punchLine;
    }

    public String getJoke() {
        return mJoke;
    }

    public String getPunchLine() {
        return mPunchLine;
    }

    public void setJoke(String joke) {
        mJoke = joke;
    }

    public void setPunchLine(String punchLine) {
        mPunchLine = punchLine;
    }
}
