package us.sensornet.jokeprovider;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class JokeProvider {

    private ArrayList<Joke> mJokes = new ArrayList<>();
    private int jokeIndex = 0;
    private Random mRnd = new Random();

    public JokeProvider() {
        // Using test data
        mJokes.addAll(jokeData);
    }

    public JokeProvider(ArrayList<Joke> jokes) {
        mJokes = jokes;
    }

    public void addJoke(Joke joke) {
        mJokes.add(joke);
    }


    public int getJokeIndex() {
        return jokeIndex;
    }

    public void removeJokeAt(int index) {
        mJokes.remove(index);
    }


    public Joke getJoke() {
        // Return a random joke
        return mJokes.get(jokeIndex);
    }


    public int nextIndex() {
        return jokeIndex = mRnd.nextInt(mJokes.size());

    }

    public String getJokeString() {
        return mJokes.get(jokeIndex).getJoke();
    }

    public String getPunchLine() {
        return mJokes.get(jokeIndex).getPunchLine();
    }

    /*
     * Randomly selects the next joke
     * from the list of jokes and returns
     * a joke object.
     *
     * You can get the just the joke text
     * by calling getJokeAsString() and
     * the punch line by calling
     * getPunchLine() or call getJOke()
     * and getPunchline() on the returned
     * joke object.
     */
    public Joke nextJoke() {
        int lastIndex = jokeIndex;
        while (lastIndex == jokeIndex) {
            jokeIndex = mRnd.nextInt(mJokes.size());
        }
        return mJokes.get(jokeIndex);
    }


    // Test data
    private List<Joke> jokeData = Arrays.asList(
            new Joke("A User Interface is like a joke,\n","If you have to explain it, it's not that good."),
            new Joke("Q: So, where did you meet?\n", "Windows Users: At the office\n\nMAC Users: At Starbucks\n\nLinux Users: On GitHub"),
            new Joke("Android:\n", "Where ProgressBars go in circles and Spinners don't!"),
            new Joke("Documentation is like sex:\n", "When it's good it's very good.\nWhen it's bad, it's better than nothing."),
            new Joke("How real men play Russian roulette:\n", "bash-4.4$ [ $[ $RANDOM % 6 ] == 0 ] && rm -rf /* || echo *click*\n(Please don't run this!)"),
            new Joke("Expected two arguments, received none:\n", "When you ask your girl to make you a sandwich and she actually does it!"),
            new Joke("Interviewer: What's your best asset?\nMe: I'm a fast learner.\nInterviewer: What's 11 x 11?", "Me: 65\nInterviewer: Not even close. It's 121.\nMe: 121"),
            new Joke("Programmer (noun.)\n", "A machine that turns coffee into code."),
            new Joke("Programmer (noun.)\n", "A person who fixed a problem you didn't know you had in a way you don't understand."),
            new Joke("Algorithm (noun.)\n", "Word used by programmers when they don't want to explain what they did."),
            new Joke("Hardware (noun.)\n", "The part of a computer you can kick!"),
            new Joke("Q: What is the object oriented way to become wealthy?\n", "Inheritence!"),
            new Joke("Q: What do you call a programmer from Finland?\n", "Nerdic!"),
            new Joke("Q: What is a programmers favorite place to hang out?\n", "Foo Bar"),
            new Joke("Q: Why did the programmer quit his job?\n", "Because he didn't get arrays"),
            new Joke("Q: 0 is false and 1 is true, right?\n", "A: 1"),
            new Joke("There are 10 kinds of people in the world:\n", "Those who understand binary and those who don't"),
            new Joke("Q: How can you tell an introvert computer scientist from an extrovert computer scientist?\n", "An extrovert computer scientist looks at your shoes when he speaks to you."),
            new Joke("Q: What do computers and airconditioners have in common?\n", "They both become useless when you open a window"),
            new Joke("Q: Why do Java programmers have to wear glasses?\n", "Because they don't C#"),
            new Joke("Q: How do you tell HTML from HTML5?\n", "- Try it out in Internet Explorer\n- Did it work?\n- No?\n- It's HTML5"),
            new Joke("Real programmers count from 0.", ""),
            new Joke("Little known fact:\n", "The word \"algorithm\" was coined to recognize Al Gore's contribution to computer science.\n(al-gore-ithm)"),
            new Joke("Chuck Norris writes code...", "that optimizes itself"),
            new Joke("Chuck Norris can take a screenshot of\n", "his blue screen."),
            new Joke("Software developers like to solve problems!\n", "If there are no problems available they will create their own!"),
            new Joke("A foo walks into a bar,\ntakes a look around...\n", "and says \"Hello World!\""),
            new Joke("A Java programmer had a problem.\nHe decided to use Java.\n", "Now he has a ProblemFactory."),
            new Joke("If you put a million monkeys at a million keyboards,\none of them will write a Java program.\n", "The rest of them will write Perl programs."),
            new Joke("99 little bugs in the code,\n99 little bug,\n1 bug fixed... compile again,", "100 little bugs in the code."),
            new Joke("3 Database SQL walked into a NoSQL bar.\nA little while later they walk out...\n", "Because they couldn't find a table."),
            new Joke("A programmer's wife sent him to the grocery store. Her instructions were: ", "\"Buy butter. See if they have eggs. If they do, buy 10\"\nSo he bought 10."),
            new Joke("The programmer got stuck in the shower\nbecause the instructions on\nthe shampoo bottle said...\n", "Lather, Rinse, Repeat."),
            new Joke("A SQL query goes into a bar,\nwalks up to two tables and asks...\n", "\"Can I join you?\""),
            new Joke("UNIX is user friendly...\n", "It's just very particular\nabout who it's friends are."),
            new Joke("Programmers don't see women as objects.\n", "They are each in a class of their own."),
            new Joke("An optimist says:\n\"the glass is half full\"\n\nA pessimist says:\n\"the glass is half empty\"\n\nA programmer says:\n\"the glass is twice as large as necessary\"", ""),
            new Joke("If the box says:\n\"This software requires windows XP or better\"\n", "Does that mean it will run on Linux?"),
            new Joke("I'd like to make the world a better place...\n", "But they won't give me the source code."),
            new Joke("Hide and seek champion...\n    ;\n", "since 1958"),
            new Joke("// life motto\nif(sad() === true) {\n   sad.stop();\n   beAwesome();\n}\n",""),
            new Joke("New data structure, Hooray:\n", "[\"hip\",\"hip\"]\n   (hip hip array!)"),
            new Joke("If yourHeart.isSet(Me) == True Then\n   MyLove += 1\nELSE\n   MyLife = null\nEND", ""),
            new Joke("#wife {\n   right 100%;\n   margin 0;\n}", "")

    );


}