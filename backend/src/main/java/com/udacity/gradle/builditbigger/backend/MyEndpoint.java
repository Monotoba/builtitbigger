package com.udacity.gradle.builditbigger.backend;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiNamespace;

import us.sensornet.jokeprovider.Joke;
import us.sensornet.jokeprovider.JokeProvider;

/** An endpoint class we are exposing */
@Api(
        name = "myApi",
        version = "v1",
        namespace = @ApiNamespace(
                ownerDomain = "backend.builditbigger.gradle.udacity.com",
                ownerName = "backend.builditbigger.gradle.udacity.com",
                packagePath = ""
        )
)
public class MyEndpoint {

    /** A simple endpoint method that returns a joke and punch line as a string */
    @ApiMethod(name = "sayHi")
    public MyBean sayHi() {
        JokeProvider jokeProvider = new JokeProvider();
        Joke joke;
        joke = jokeProvider.nextJoke();
        MyBean response = new MyBean();
        response.setData(joke.getJoke() + "\n" + joke.getPunchLine());
        return response;
    }

}
