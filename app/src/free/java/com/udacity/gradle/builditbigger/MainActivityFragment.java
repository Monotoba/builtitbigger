package com.udacity.gradle.builditbigger;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import us.sensornet.jokedisplay.Constants.Constants;
import us.sensornet.jokedisplay.JokeDisplayActivity;

/**
 * A placeholder fragment containing a simple view.
 */
public class MainActivityFragment extends Fragment implements EndpointAsyncTask.EndpointListener {
    private static final String TAG = "MainActivityFragment";

    String strJoke = "";
    ProgressBar pbSpinner;
    Button btnJoke;
    AdView mAdView;


    //private MyApi apiService = null;

    public MainActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d(TAG, "Free Ver. onCreateView: called");

        View root = inflater.inflate(R.layout.fragment_main, container, false);

        Log.d(TAG, "Free Ver. onCreateView: Loaded Layout");

        AdView mAdView = (AdView) root.findViewById(R.id.adView);
        // Create an ad request. Check logcat output for the hashed device ID to
        // get test ads on a physical device. e.g.
        // "Use AdRequest.Builder.addTestDevice("ABCDEF012345") to get test ads on this device."
        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                .build();
        mAdView.loadAd(adRequest);

        Log.d(TAG, "Free Ver. onCreateView: Loaded Ads");

        pbSpinner = root.findViewById(R.id.pb_progress_bar);
        btnJoke = root.findViewById(R.id.btn_joke);
        btnJoke.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tellJoke();
            }
        });

        Log.d(TAG, "Free Ver. onCreateView: completed");

        return root;
    }

    public void tellJoke() {

        EndpointAsyncTask mEndpoint = new EndpointAsyncTask();
        mEndpoint.setListener(this);
        mEndpoint.execute(getActivity());

    }


    private void displayJoke() {
        if(null != strJoke) {
            Intent intent = new Intent(getActivity(), JokeDisplayActivity.class);
            intent.putExtra(Constants.JOKE_TEXT, strJoke);
            getActivity().startActivity(intent);
        } else {
            Toast.makeText(getContext(), R.string.str_error_message, Toast.LENGTH_LONG);
        }
    }


    // Interface Methods
    @Override
    public void onResult(String joke) {
        strJoke = joke;
        displayJoke();
    }

    @Override
    public void onError(String error) {
        Log.d(TAG, "MainFragment: onError called with - " + error);
        strJoke = error;
    }

    @Override
    public void onTaskPreExecute() {
        pbSpinner.setVisibility(View.VISIBLE);
    }

    @Override
    public void onTaskPostExecute() {
        pbSpinner.setVisibility(View.GONE);
    }
}
