package com.udacity.gradle.builditbigger;

import android.app.Application;
import android.test.ApplicationTestCase;

import org.junit.Test;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/**
 * <a href="http://d.android.com/tools/testing/testing_android.html">Testing Fundamentals</a>
 */
public class ApplicationTest extends ApplicationTestCase<Application> implements EndpointAsyncTask.EndpointListener {

    private String joke;
    private CountDownLatch signal;

    public ApplicationTest() {
        super(Application.class);
    }

    @Test
    public void requestResponseText() {
        try {
            signal = new CountDownLatch(1);
            EndpointAsyncTask task = new EndpointAsyncTask();
            task.setListener(this);
            task.execute();
            signal.await(30, TimeUnit.SECONDS);
            assertNotNull("EndpointAsyncTask Request Failed: returned null ", joke);
            assertFalse("EndpointAsyncTask Request Failed: is empty", joke.isEmpty());
        } catch (Exception e) {
            fail();
        }
    }

    @Override
    public void onResult(String result) {
        joke = result;
        signal.countDown();
    }

    @Override
    public void onError(String error) {
        fail();
    }

    @Override
    public void onTaskPreExecute() {
        // Do nothing
    }

    @Override
    public void onTaskPostExecute() {
        // Do nothing
    }
}
