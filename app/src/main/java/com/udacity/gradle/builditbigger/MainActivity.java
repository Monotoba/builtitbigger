package com.udacity.gradle.builditbigger;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import us.sensornet.jokedisplay.Constants.Constants;
import us.sensornet.jokedisplay.JokeDisplayActivity;
import us.sensornet.jokeprovider.Joke;
import us.sensornet.jokeprovider.JokeProvider;

public class MainActivity extends AppCompatActivity {

    private JokeProvider joker = new JokeProvider();
    private Joke joke;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        joker = new JokeProvider();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void tellJoke(View view){
        joke = joker.nextJoke();
//        Toast.makeText(this, joke.getJoke() + "\n" + joke.getPunchLine(), Toast.LENGTH_LONG).show();

        Intent intent = new Intent(this, JokeDisplayActivity.class);
        intent.putExtra(Constants.JOKE_TEXT, joke.getJoke());
        intent.putExtra(Constants.PUNCHLINE_TEXT, joke.getPunchLine());
        startActivity(intent);
    }


}
