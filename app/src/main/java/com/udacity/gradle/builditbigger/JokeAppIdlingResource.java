package com.udacity.gradle.builditbigger;

import android.support.test.espresso.IdlingResource;
import android.support.test.espresso.idling.CountingIdlingResource;

public class JokeAppIdlingResource {
    private static final String IDLING_RESOURCE = "IDLING_RESOURCE";


    private static CountingIdlingResource mCountingIdlingResource = new CountingIdlingResource(IDLING_RESOURCE);

    public static void increment() {
        mCountingIdlingResource.increment();
    }

    public static void decrement() {
        mCountingIdlingResource.decrement();
    }

    public static IdlingResource getIdlingResource() {
        return mCountingIdlingResource;
    }
}
