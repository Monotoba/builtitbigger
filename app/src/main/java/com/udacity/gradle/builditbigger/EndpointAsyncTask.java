package com.udacity.gradle.builditbigger;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.extensions.android.json.AndroidJsonFactory;
import com.google.api.client.googleapis.services.AbstractGoogleClientRequest;
import com.google.api.client.googleapis.services.GoogleClientRequestInitializer;
import com.udacity.gradle.builditbigger.backend.myApi.MyApi;

import java.io.IOException;

public class EndpointAsyncTask extends AsyncTask<Context, Void, String> {
    private static final String TAG = "EndpointAsyncTask";

    private EndpointListener mListener;
    private MyApi apiService;

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        // Idling resource here
        JokeAppIdlingResource.increment();
        mListener.onTaskPreExecute();
    }

    @Override
    protected String doInBackground(Context... params) {
        // create the apiService
        if(null == apiService) {
            MyApi.Builder builder = new MyApi.Builder(AndroidHttp.newCompatibleTransport(),
                    new AndroidJsonFactory(), null)
                    .setRootUrl("http://10.0.2.2:8080/_ah/api/")
                    .setGoogleClientRequestInitializer(new GoogleClientRequestInitializer() {
                                                           @Override
                                                           public void initialize(AbstractGoogleClientRequest<?> request) throws IOException {
                                                               request.setDisableGZipContent(true);
                                                           }
                                                       }
                    ).setApplicationName("BuildItBigger");

            apiService = builder.build();
        }

        // Run the request
        try {
            String data = apiService.sayHi().execute().getData();
            if(null == data || data.isEmpty()) Log.d(TAG, "doInBackground: Joke data is null or empty!");
            mListener.onResult(data);
        } catch (IOException e) {
            Log.d(TAG, "doInBackground: Error: " + e.getMessage());
            mListener.onError(e.getMessage());
        }

        return null;
    }


    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        JokeAppIdlingResource.decrement();
        mListener.onTaskPostExecute();
    }

    public void setListener(EndpointListener listener) {
        mListener = listener;
    }


    public interface EndpointListener {
        void onResult(String joke);
        void onError(String error);
        void onTaskPreExecute();
        void onTaskPostExecute();
    }
}