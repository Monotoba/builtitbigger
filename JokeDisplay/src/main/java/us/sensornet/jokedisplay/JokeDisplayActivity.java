package us.sensornet.jokedisplay;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import us.sensornet.jokedisplay.Constants.Constants;

public class JokeDisplayActivity extends AppCompatActivity {


    private String mJoke = "";
    private String mPunchLine = "";
    private Bundle mData;
    private TextView tvJoke;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_joke_display);

        mData = getIntent().getExtras();

        if (null != mData) {
            mJoke = mData.getString(Constants.JOKE_TEXT);
            mPunchLine = mData.getString(Constants.PUNCHLINE_TEXT);
            if(null == mPunchLine) mPunchLine = "";
        }

        if (null == mData || null == mJoke) {
            mJoke = "Uh Oh! No Joke found. No Joking here...";
            mPunchLine = "";
        }

        tvJoke = findViewById(R.id.tv_joke_text);
        String msg = mJoke + "\n" + mPunchLine;
        tvJoke.setText(msg);

    }
}